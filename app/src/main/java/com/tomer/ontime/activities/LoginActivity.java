package com.tomer.ontime.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tomer.ontime.R;
import com.tomer.ontime.app.OnTimeApplication;
import com.tomer.ontime.assignments_getters.MoodleAssignmentsGetter;
import com.tomer.ontime.utils.SharedPreferencesHelper;
import com.tomer.ontime.utils.TimeConstants;
import com.tomer.ontime.utils.Utils;

import org.jsoup.helper.StringUtil;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity {

    private static final String WRONG_LOGIN_CREDENTIALS_MESSAGE = "Wrong login credentials";
    private static final String USER_CREDENTIALS_CHECK_FAILURE_MESSAGE = "User credentials check failed. " +
            "if the credentials are wrong, new assignments from login required systems (Like Moodle) will not show";
    private static final String USERNAME_PASSWORD_WERE_NOT_ENTERED_MESSAGE = "Username/Password were not entered";
    private static final String INVALID_USERNAME_ENTERED_MESSAGE = "Invalid username was entered.\nUsername should be 9 digits.";
    private static final String VALIDATING_LOGIN_CREDENTIALS_MESSAGE = "Validating login credentials.\nwill take up to "
            + (CredentialsCheckAsyncTask.CREDENTIALS_CHECK_TIMEOUT_MILLIS/TimeConstants.MILLIS_IN_A_SECOND) + " seconds";

    private TextView credentialsValidationTextView;
    private ProgressBar credentialsValidationProgressBar;
    private EditText idEditText;
    private EditText passEditText;
    private Button okButton;
    private SharedPreferencesHelper sharedPrefsHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        credentialsValidationTextView = (TextView) findViewById(R.id.credentialsValidationTextView);
        credentialsValidationProgressBar = (ProgressBar) findViewById(R.id.credentialsValidationProgressBar);
        idEditText = (EditText) findViewById(R.id.idEditText);
        passEditText = (EditText) findViewById(R.id.passEditText);
        passEditText.setTransformationMethod(new PasswordTransformationMethod());
        okButton = (Button) findViewById(R.id.button);
        sharedPrefsHelper = ((OnTimeApplication) getApplicationContext()).getSharedPrefsHelper();
        loadUserCredentials();
        onClickOKButton(null);
    }

    private void setCredentialsCheckViews(String textViewMessage,
                                          int textViewVisibility,
                                          int progressBarVisibility,
                                          boolean okButtonEnabled) {
        credentialsValidationTextView.setText(textViewMessage);
        credentialsValidationTextView.setVisibility(textViewVisibility);
        credentialsValidationProgressBar.setVisibility(progressBarVisibility);
        okButton.setEnabled(okButtonEnabled);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_contact_me: {
                Utils.sendEmail(new String[]{"tomerr90@gmail.com"}, "[OnTime] User experience", this);
                return true;
            }
            case R.id.action_settings: {
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            }
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveUserCredentials(String username, String password) {
        sharedPrefsHelper.setUsername(username);
        sharedPrefsHelper.setPassword(password);
    }

    private void loadUserCredentials() {
        idEditText.setText(sharedPrefsHelper.getUsername());
        passEditText.setText(sharedPrefsHelper.getPassword());
    }

    private void handleCredentialsCheckResponse(CredentialsCheckResponse response) {
        switch (response){
            case ERROR_OCCURRED: {
                Toast.makeText(this, USER_CREDENTIALS_CHECK_FAILURE_MESSAGE, Toast.LENGTH_LONG).show();
                startAssignmentsTrackingActivity();
                return;
            }
            case VALID: {
                startAssignmentsTrackingActivity();
                return;
            }
            case WRONG: {
                setCredentialsCheckViews(WRONG_LOGIN_CREDENTIALS_MESSAGE, View.VISIBLE, View.INVISIBLE, true);
            }
            default: break;
        }
    }

    private void validateUserCredentials(String username, String password){
        if (StringUtil.isBlank(username) || StringUtil.isBlank(password)){
            Toast.makeText(this, USERNAME_PASSWORD_WERE_NOT_ENTERED_MESSAGE, Toast.LENGTH_LONG).show();
            return;
        }
        if (username.length() != 9){
            Toast.makeText(this, INVALID_USERNAME_ENTERED_MESSAGE, Toast.LENGTH_LONG).show();
            return;
        }
        setCredentialsCheckViews(VALIDATING_LOGIN_CREDENTIALS_MESSAGE, View.VISIBLE, View.VISIBLE, false);
        new CredentialsCheckAsyncTask().execute(username, password);
    }

    public void onClickOKButton(View view) {
        String username = idEditText.getText().toString();
        String password = passEditText.getText().toString();
        saveUserCredentials(username, password);
        validateUserCredentials(username, password);
    }

    private void startAssignmentsTrackingActivity(){
        startActivity(new Intent(this, AssignmentsTrackingActivity.class));
        finish();
    }

    private enum CredentialsCheckResponse {
        VALID, ERROR_OCCURRED, WRONG
    }

    private class CredentialsCheckAsyncTask extends AsyncTask<String, Void, CredentialsCheckResponse> {

        public static final long CREDENTIALS_CHECK_TIMEOUT_MILLIS = 10 * TimeConstants.MILLIS_IN_A_SECOND;
        private static final String BAD_USER_CREDENTIALS_HTML_INDICATOR = "login/index.php";

        @Override
        protected CredentialsCheckResponse doInBackground(String... params) {
            CredentialsCheckWorkerThread workerThread = new CredentialsCheckWorkerThread(params[0], params[1]);
            workerThread.start();
            waitForCredentialCheckToFinishOrTimeout(workerThread);
            workerThread.interrupt();
            return workerThread.response;
        }

        private void waitForCredentialCheckToFinishOrTimeout(CredentialsCheckWorkerThread workerThread) {
            long startTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startTime < CREDENTIALS_CHECK_TIMEOUT_MILLIS
                    && workerThread.getState() != Thread.State.TERMINATED) {
                try {
                    Thread.sleep(TimeConstants.MILLIS_IN_A_SECOND);
                } catch (InterruptedException e) {}
            }
        }

        @Override
        protected void onPostExecute(CredentialsCheckResponse response) {
            handleCredentialsCheckResponse(response);
        }

        private class CredentialsCheckWorkerThread extends Thread{
            private CredentialsCheckResponse response = CredentialsCheckResponse.ERROR_OCCURRED;
            private String username;
            private String pass;

            public CredentialsCheckWorkerThread(String username, String pass){
                this.username = username;
                this.pass = pass;
            }

            @Override
            public void run() {
                try {
                    MoodleAssignmentsGetter.loginToMoodle(username, pass);
                    String pageHtml = Utils.doHttpGet(MoodleAssignmentsGetter.MOODLE_URL);
                    response = pageHtml.contains(BAD_USER_CREDENTIALS_HTML_INDICATOR) ?
                            CredentialsCheckResponse.WRONG : CredentialsCheckResponse.VALID;
                } catch (IOException e) {
                    Log.e(getClass().getSimpleName(), "Failed to access UG", e);
                }
            }
        }
    }
}
