package com.tomer.ontime.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.tomer.ontime.R;
import com.tomer.ontime.adapters.TodoDoneFragmentPagerAdapter;
import com.tomer.ontime.broadcast_receivers.AssignmentsGetterIntentServiceReceiver;
import com.tomer.ontime.broadcast_receivers.AssignmentsReminderIntentServiceReceiver;
import com.tomer.ontime.services.AssignmentsGetterIntentService;
import com.tomer.ontime.utils.TimeConstants;
import com.tomer.ontime.utils.Utils;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Tomer on 10-Feb-16.
 */
public class AssignmentsTrackingActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignments_tracking);
        PagerAdapter pagerAdapter = new TodoDoneFragmentPagerAdapter(getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark);

        scheduleServicesToRun();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_contact_me: {
                Utils.sendEmail(new String[]{"tomerr90@gmail.com"}, "[OnTime] User experience", this);
                return true;
            }
            case R.id.action_settings: {
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            }
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void scheduleServicesToRun() {
        AssignmentsGetterIntentServiceReceiver.scheduleIntentServiceToRun(getApplicationContext());
        AssignmentsReminderIntentServiceReceiver.scheduleIntentServiceToRun(getApplicationContext());
    }

    @Override
    public void onRefresh() {
        new RefreshAssignmentsListThread().start();
    }

    private class RefreshAssignmentsListThread extends Thread{
        @Override
        public void run() {
            if (!AssignmentsGetterIntentService.processingIntent.get()){
                startService(new Intent(AssignmentsTrackingActivity.this, AssignmentsGetterIntentService.class));
                waitForServiceToStart();
            }
            waitForServiceToFinish();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            });
        }

        private void waitForServiceToFinish() {
            waitForVariableToHaveValue(AssignmentsGetterIntentService.processingIntent, false);
        }

        private void waitForServiceToStart() {
            waitForVariableToHaveValue(AssignmentsGetterIntentService.processingIntent, true);
        }

        private void waitForVariableToHaveValue(final AtomicBoolean var, boolean val) {
            while(var.get() != val){
                try {
                    Thread.sleep(TimeConstants.MILLIS_IN_A_SECOND);
                } catch (InterruptedException e) {}
            }
        }
    }
}