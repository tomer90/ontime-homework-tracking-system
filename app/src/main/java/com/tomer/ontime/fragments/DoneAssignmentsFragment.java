package com.tomer.ontime.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tomer.ontime.adapters.AssignmentsRecyclerAdapter;
import com.tomer.ontime.app.OnTimeApplication;
import com.tomer.ontime.utils.AssignmentsDBWrapper;

/**
 * Created by Tomer on 07-Jun-16.
 */
public class DoneAssignmentsFragment extends AssignmentsBaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        updateAssignmentsRecyclerView();
        return v;
    }

    private void updateAssignmentsRecyclerView() {
        OnTimeApplication appContext = (OnTimeApplication) getActivity().getApplicationContext();
        AssignmentsDBWrapper assignmentsDBWrapper = appContext.getAssignmentsDBWrapper();
        AssignmentsRecyclerAdapter assignmentsRecyclerAdapter = new AssignmentsRecyclerAdapter(
                getActivity(),
                assignmentsDBWrapper.getDoneAssignments(),
                AssignmentsRecyclerAdapter.AssignmentsType.DONE);
        assignmentsRecyclerView.setAdapter(assignmentsRecyclerAdapter);
        assignmentsDBWrapper.close();
    }

    @Override
    protected void assignmentsDBChangedCallback() {
        updateAssignmentsRecyclerView();
    }
}
