package com.tomer.ontime.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tomer.ontime.R;

/**
 * Created by Tomer on 09-Jun-16.
 */
public abstract class AssignmentsBaseFragment extends Fragment{

    private AssignmentsDBChangedListener dbChangedListener;
    protected RecyclerView assignmentsRecyclerView;

    protected abstract void assignmentsDBChangedCallback();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.assignments_display_fragment_layout, container, false);
        assignmentsRecyclerView = (RecyclerView) rootView.findViewById(R.id.assignmentsRecyclerView);
        assignmentsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbChangedListener = new AssignmentsDBChangedListener();
        getActivity().registerReceiver(dbChangedListener, new IntentFilter(AssignmentsDBChangedListener.ACTION_ASSIGNMENTS_DB_CHANGED));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getActivity().unregisterReceiver(dbChangedListener);
        } catch (Exception e){}
    }

    public class AssignmentsDBChangedListener extends BroadcastReceiver {
        public static final String ACTION_ASSIGNMENTS_DB_CHANGED = "assignmentsDBChanged";

        @Override
        public void onReceive(Context context, Intent intent) {
            assignmentsDBChangedCallback();
        }
    }
}
