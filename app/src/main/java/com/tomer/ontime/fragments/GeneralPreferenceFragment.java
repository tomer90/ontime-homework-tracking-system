package com.tomer.ontime.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.tomer.ontime.R;
import com.tomer.ontime.activities.SettingsActivity;
import com.tomer.ontime.app.OnTimeApplication;
import com.tomer.ontime.services.AssignmentsGetterIntentService;
import com.tomer.ontime.utils.AssignmentsDBWrapper;
import com.tomer.ontime.utils.SharedPreferencesHelper;
import com.tomer.ontime.utils.Utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tomer on 14-Jun-16.
 */

public class GeneralPreferenceFragment extends PreferenceFragment {

    private Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new PreferenceSummaryUpdateListener();
    private SharedPreferencesHelper sharedPrefsHelper;
    OnTimeApplication appContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_general);
        setHasOptionsMenu(true);
        appContext = (OnTimeApplication) getActivity().getApplicationContext();
        sharedPrefsHelper = appContext.getSharedPrefsHelper();
        bindPreferenceSummaryToValue(findPreference(sharedPrefsHelper.getUsernameKey()));
        bindPreferenceSummaryToValue(findPreference(sharedPrefsHelper.getPasswordKey()));
        bindPreferenceSummaryToValue(findPreference(sharedPrefsHelper.getReminderTimesListKey()));
        CheckBoxPreference detectEnrolledCourses = (CheckBoxPreference) findPreference(sharedPrefsHelper.getShouldDetectEnrolledCoursesKey());
        final Preference enrolledCoursesPref = findPreference(sharedPrefsHelper.getEnrolledCoursesKey());
        enrolledCoursesPref.setEnabled(!detectEnrolledCourses.isChecked());
        enrolledCoursesPref.setOnPreferenceChangeListener(new EnrolledCoursesPrefListener());
        enrolledCoursesPref.setSummary(sharedPrefsHelper.getEnrolledCoursesAsString());
        detectEnrolledCourses.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                enrolledCoursesPref.setEnabled(!((Boolean) newValue));
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            startActivity(new Intent(getActivity(), SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
        if (preference instanceof MultiSelectListPreference) {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(appContext)
                            .getStringSet(preference.getKey(), new HashSet<String>()));
        } else {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(appContext)
                            .getString(preference.getKey(), ""));
        }
    }

    private class EnrolledCoursesPrefListener implements Preference.OnPreferenceChangeListener {
        private static final String INVALID_COURSE_NUMBERS_MESSAGE = "Found some invalid course numbers, please fix them, ignored your change.";
        private static final int VALID_COURSE_LENGTH = 6;
        private static final String ASSIGNMENTS_GETTER_STARTED_MESSAGE = "Updating assignments, will take about a minute.";

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if (sharedPrefsHelper.getEnrolledCoursesAsString().equals(newValue)){
                return false;
            }
            Set<String> newEnrolledCourses = sharedPrefsHelper.getEnrolledCoursesFromEnrolledCoursesString((String) newValue);
            if (!areCourseNumbersValid(newEnrolledCourses)) {
                Toast.makeText(getActivity(), INVALID_COURSE_NUMBERS_MESSAGE, Toast.LENGTH_SHORT).show();
                return false;
            }
            updateAssignments();
            preference.setSummary(newValue.toString());
            return true;
        }

        private void updateAssignments() {
            appContext.startService(new Intent(appContext, AssignmentsGetterIntentService.class));
            Toast.makeText(getActivity(), ASSIGNMENTS_GETTER_STARTED_MESSAGE, Toast.LENGTH_LONG).show();
        }

        private boolean areCourseNumbersValid(Set<String> courseNums) {
            for (String courseNum : courseNums) {
                if (courseNum.length() != VALID_COURSE_LENGTH || !Utils.containsOnlyDigits(courseNum)) {
                    return false;
                }
            }
            return true;
        }
    }

    private class PreferenceSummaryUpdateListener implements Preference.OnPreferenceChangeListener {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            if (preference.getKey().equals(sharedPrefsHelper.getPasswordKey())){
                value = maskString((EditTextPreference) preference, value);
            }
            preference.setSummary(value.toString());
            return true;
        }

        private String maskString(EditTextPreference preference, Object value) {
            EditText editText = preference.getEditText();
            return editText.getTransformationMethod().getTransformation(value.toString(), editText).toString();
        }
    }
}
