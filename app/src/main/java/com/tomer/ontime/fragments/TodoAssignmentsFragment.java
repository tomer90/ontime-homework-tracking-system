package com.tomer.ontime.fragments;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tomer.ontime.adapters.AssignmentsRecyclerAdapter;
import com.tomer.ontime.app.OnTimeApplication;
import com.tomer.ontime.utils.Assignment;
import com.tomer.ontime.utils.AssignmentsDBWrapper;
import com.tomer.ontime.utils.TimeConstants;
import com.tomer.ontime.utils.Utils;

import java.util.Date;
import java.util.List;

/**
 * Created by Tomer on 07-Jun-16.
 */
public class TodoAssignmentsFragment extends AssignmentsBaseFragment {

    private AssignmentsRecyclerAdapter assignmentsRecyclerAdapter;
    private AssignmentsRecyclerViewUpdateTimer assignmentsRecyclerViewUpdateTimer;
    private AssignmentsDBWrapper assignmentsDBWrapper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (assignmentsDBWrapper == null){
            OnTimeApplication appContext = (OnTimeApplication) getActivity().getApplicationContext();
            assignmentsDBWrapper = appContext.getAssignmentsDBWrapper();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        assignmentsDBChangedCallback();
        return v;
    }

    @Override
    protected void assignmentsDBChangedCallback() {
        moveOverdueAssignmentsToDoneInDB();
        updateAssignmentsRecyclerView();
    }

    private boolean moveOverdueAssignmentsToDoneInDB() {
        boolean foundOverdueAssignments = false;
        for (Assignment assignment : assignmentsDBWrapper.getTodoAssignments()){
            Date assignmentDueDate = assignment.getDueDate();
            if (assignmentDueDate != null
                    && assignmentDueDate.getTime() - System.currentTimeMillis() < TimeConstants.MILLIS_IN_A_MINUTE){
                assignmentsDBWrapper.moveAssignmentToDone(assignment);
                foundOverdueAssignments = true;
            }
        }
        return foundOverdueAssignments;
    }

    private void updateAssignmentsRecyclerView() {
        List<Assignment> todoAssignments = assignmentsDBWrapper.getTodoAssignments();
        assignmentsRecyclerAdapter = new AssignmentsRecyclerAdapter(
                getActivity(),
                todoAssignments,
                AssignmentsRecyclerAdapter.AssignmentsType.TODO);
        assignmentsRecyclerView.setAdapter(assignmentsRecyclerAdapter);
        resetUpdateTimer(getLatestDueDateMillis(todoAssignments));
    }

    private void resetUpdateTimer(long millisInFuture) {
        if (assignmentsRecyclerViewUpdateTimer != null){
            assignmentsRecyclerViewUpdateTimer.cancel();
        }
        assignmentsRecyclerViewUpdateTimer = new AssignmentsRecyclerViewUpdateTimer(millisInFuture, TimeConstants.MILLIS_IN_A_MINUTE);
        assignmentsRecyclerViewUpdateTimer.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        assignmentsRecyclerViewUpdateTimer.cancel();
        assignmentsDBWrapper.close();
    }

    private long getLatestDueDateMillis(List<Assignment> assignments) {
        long latestDueDateMillis = Long.MIN_VALUE;
        for (Assignment assignment : assignments){
            Date assignmentDueDate = assignment.getDueDate();
            if (assignmentDueDate != null) {
                latestDueDateMillis = Math.max(latestDueDateMillis, assignmentDueDate.getTime());
            }
        }
        return Math.max(1000, latestDueDateMillis);
    }

    public class AssignmentsRecyclerViewUpdateTimer extends CountDownTimer {

        public AssignmentsRecyclerViewUpdateTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (moveOverdueAssignmentsToDoneInDB()){
                Utils.alertAssignmentsDBChanged(getActivity());
            } else {
                assignmentsRecyclerAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onFinish() {}
    }
}
