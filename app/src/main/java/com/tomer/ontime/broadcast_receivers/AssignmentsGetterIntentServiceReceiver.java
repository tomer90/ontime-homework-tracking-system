package com.tomer.ontime.broadcast_receivers;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.tomer.ontime.services.AssignmentsGetterIntentService;
import com.tomer.ontime.utils.TimeConstants;
import com.tomer.ontime.utils.Utils;

public class AssignmentsGetterIntentServiceReceiver extends BroadcastReceiver {

    public static final long REPEAT_INTERVAL_MILLIS = 8 * TimeConstants.MILLIS_IN_AN_HOUR;

    @Override
    public void onReceive(Context context, Intent intent) {
        scheduleIntentServiceToRun(context);
    }

    public static void scheduleIntentServiceToRun(Context context) {
        Intent intent = new Intent(context, AssignmentsGetterIntentService.class);
        Utils.scheduleRepeatingIntentIfNotScheduled(context, AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                REPEAT_INTERVAL_MILLIS, intent);
    }
}
