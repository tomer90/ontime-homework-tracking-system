package com.tomer.ontime.broadcast_receivers;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.tomer.ontime.services.AssignmentsReminderIntentService;
import com.tomer.ontime.utils.Utils;

import java.util.Calendar;

public class AssignmentsReminderIntentServiceReceiver extends BroadcastReceiver {
    public static final int HOUR_OF_DAY_TO_RUN = 12;

    @Override
    public void onReceive(Context context, Intent intent) {
        scheduleIntentServiceToRun(context);
    }

    public static void scheduleIntentServiceToRun(Context context) {
        Intent intent = new Intent(context, AssignmentsReminderIntentService.class);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, HOUR_OF_DAY_TO_RUN);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Utils.scheduleRepeatingIntentIfNotScheduled(context, AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, intent);
    }
}
