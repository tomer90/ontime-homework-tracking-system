package com.tomer.ontime.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.Pair;

import com.tomer.ontime.R;
import com.tomer.ontime.activities.LoginActivity;
import com.tomer.ontime.app.OnTimeApplication;
import com.tomer.ontime.assignments_getters.IAssignmentsGetter;
import com.tomer.ontime.assignments_getters.MoodleAssignmentsGetter;
import com.tomer.ontime.assignments_getters.PHMoodleAssignmentsGetter;
import com.tomer.ontime.assignments_getters.WebCourseAssignmentsGetter;
import com.tomer.ontime.utils.Assignment;
import com.tomer.ontime.utils.AssignmentsDBWrapper;
import com.tomer.ontime.utils.EnrolledCoursesHandler;
import com.tomer.ontime.utils.SharedPreferencesHelper;
import com.tomer.ontime.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Tomer on 09-Jan-16.
 */
public class AssignmentsGetterIntentService extends WakefulIntentService {

    private static final String NEW_ASSIGNMENTS_NOTIFICATION_TEXT = "Better get started!";
    private static final String NEW_ASSIGNMENTS_NOTIFICATION_TITLE = " New Assignments";
    private static final String TAG = AssignmentsGetterIntentService.class.getSimpleName();
    private static final int FOREGROUND_SERVICE_NOTIFICATION_ID = 1;

    public static AtomicBoolean processingIntent = new AtomicBoolean(false);
    private boolean dbChanged;
    private AssignmentsDBWrapper assignmentsDBWrapper;
    private SharedPreferencesHelper sharedPrefsHelper;
    private EnrolledCoursesHandler enrolledCoursesHandler;

    private HashSet<IAssignmentsGetter> assignmentsGetters = new HashSet<>(
            Arrays.asList(
                    new MoodleAssignmentsGetter(),
                    new PHMoodleAssignmentsGetter(),
                    new WebCourseAssignmentsGetter()
            )
    );

    public AssignmentsGetterIntentService() {
        super(AssignmentsGetterIntentService.class.getName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OnTimeApplication appContext = (OnTimeApplication) getApplicationContext();
        assignmentsDBWrapper = appContext.getAssignmentsDBWrapper();
        sharedPrefsHelper = appContext.getSharedPrefsHelper();
        enrolledCoursesHandler = new EnrolledCoursesHandler(sharedPrefsHelper, assignmentsDBWrapper);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        assignmentsDBWrapper.close();
        stopServiceForeground();
    }

    private void startServiceForeground() {
        startForeground(FOREGROUND_SERVICE_NOTIFICATION_ID, buildUpdatingAssignmentsNotification(0));
    }

    private void UpdateProgressInNotification(int progress){
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(FOREGROUND_SERVICE_NOTIFICATION_ID, buildUpdatingAssignmentsNotification(progress));
    }

    private void stopServiceForeground() {
        stopForeground(true);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent started");
        processingIntent.set(true);
        startServiceForeground();

        dbChanged = enrolledCoursesHandler.handleEnrolledCourses();
        Pair<ArrayList<Assignment>, ArrayList<Assignment>> newTodoAndDoneAssignments = getNewTodoAndDoneAssignments();
        addNewAssignmentsToDB(newTodoAndDoneAssignments);
        int numOfNewAssignments = newTodoAndDoneAssignments.first.size();
        if (numOfNewAssignments > 0) {
            showNewAssignmentsNotification(numOfNewAssignments);
        }
        if (dbChanged){
            Utils.alertAssignmentsDBChanged(this);
        }

        stopServiceForeground();
        processingIntent.set(false);
        Log.d(TAG, "onHandleIntent finished");
    }

    private void showNewAssignmentsNotification(int numOfNewAssignments) {
        Intent resultIntent = new Intent(getApplicationContext(), LoginActivity.class);
        Utils.showNotification(getApplicationContext(),
                PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT),
                numOfNewAssignments + NEW_ASSIGNMENTS_NOTIFICATION_TITLE,
                NEW_ASSIGNMENTS_NOTIFICATION_TEXT);
        dbChanged = true;
    }

    private void addNewAssignmentsToDB(Pair<ArrayList<Assignment>, ArrayList<Assignment>> newTodoAndDoneAssignments) {
        assignmentsDBWrapper.addTodoAssignments(newTodoAndDoneAssignments.first);
        assignmentsDBWrapper.addDoneAssignments(newTodoAndDoneAssignments.second);
    }

    //OverDue assignments will be considered as Done, as the user can do nothing about them now...
    private Pair<ArrayList<Assignment>, ArrayList<Assignment>> getNewTodoAndDoneAssignments() {
        ArrayList<Assignment> newAssignments = getAssignments();
        Pair<ArrayList<Assignment>, ArrayList<Assignment>> doneAndTodoAssignments = splitToDoneAndTodoAssignments(newAssignments);
        ArrayList<Assignment> newDoneAssignments = doneAndTodoAssignments.first;
        ArrayList<Assignment> newTodoAssignments = doneAndTodoAssignments.second;
        handleChangedDueDates(newTodoAssignments);
        removeMarkedDoneAssignments(newTodoAssignments);
        return new Pair<>(newTodoAssignments, newDoneAssignments);
    }

    private void removeMarkedDoneAssignments(ArrayList<Assignment> newTodoAssignments) {
        for (Assignment doneAssignment : assignmentsDBWrapper.getDoneAssignments()){
            doneAssignment.markNotDone();
            if (newTodoAssignments.contains(doneAssignment)){
                newTodoAssignments.remove(doneAssignment);
            }
        }
    }

    private void handleChangedDueDates(ArrayList<Assignment> newTodoAssignments) {
        for (Assignment todoAssignment : assignmentsDBWrapper.getTodoAssignments()){
            if (todoAssignment.getDueDate() == null || !newTodoAssignments.contains(todoAssignment)){
                continue;
            }
            Assignment newTodoAssignment = newTodoAssignments.get(newTodoAssignments.indexOf(todoAssignment));
            if (todoAssignment.getDueDate().equals(newTodoAssignment.getDueDate())){
                newTodoAssignments.remove(newTodoAssignment);
            } else {
                assignmentsDBWrapper.deleteTodoAssignment(todoAssignment);
                dbChanged = true;
            }
        }
    }
    
    private ArrayList<Assignment> getAssignments() {
        ArrayList<Assignment> newAssignments = new ArrayList<>();
        List<AssignmentCollectingAsyncTask> tasks = new ArrayList<>();
        for (IAssignmentsGetter assignmentsGetter : assignmentsGetters) {
            AssignmentCollectingAsyncTask task = new AssignmentCollectingAsyncTask();
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, assignmentsGetter);
            tasks.add(task);
        }
        int numOfFinishedGetters = 0;
        for (AssignmentCollectingAsyncTask task : tasks){
            while (true){
                try {
                    newAssignments.addAll(task.get());
                    break;
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    break;
                } catch (InterruptedException e) {}
            }
            numOfFinishedGetters++;
            UpdateProgressInNotification(numOfFinishedGetters);
        }
        return newAssignments;
    }

    @NonNull
    private Notification buildUpdatingAssignmentsNotification(int progress) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.app_icon);
        notificationBuilder.setDefaults(0)
                .setSmallIcon(R.drawable.app_icon)
                .setLargeIcon(icon)
                .setContentTitle("Updating Assignments...")
                .setAutoCancel(true)
                .setContentText(String.valueOf((100*progress / assignmentsGetters.size())) + "%")
                .setProgress(assignmentsGetters.size(), progress, false);
        return notificationBuilder.build();
    }

    private Pair<ArrayList<Assignment>, ArrayList<Assignment>> splitToDoneAndTodoAssignments(ArrayList<Assignment> assignments) {
        ArrayList<Assignment> doneAssignments = new ArrayList<>();
        ArrayList<Assignment> todoAssignments = new ArrayList<>();
        Date current = new Date();
        for (Assignment assignment : assignments) {
            Date assignmentDueDate = assignment.getDueDate();
            if (assignmentDueDate == null || current.before(assignmentDueDate)) {
                assignment.markNotDone();
                todoAssignments.add(assignment);
            } else {
                assignment.markDone();
                doneAssignments.add(assignment);
            }
        }
        return new Pair<>(doneAssignments, todoAssignments);
    }

    public class AssignmentCollectingAsyncTask extends AsyncTask<IAssignmentsGetter, Void, List<Assignment>> {

        @Override
        protected List<Assignment> doInBackground(IAssignmentsGetter... params) {
            return params[0].getAssignments(sharedPrefsHelper);
        }

    }
}
