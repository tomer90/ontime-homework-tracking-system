package com.tomer.ontime.services;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.Pair;

import com.tomer.ontime.activities.LoginActivity;
import com.tomer.ontime.app.OnTimeApplication;
import com.tomer.ontime.utils.Assignment;
import com.tomer.ontime.utils.AssignmentsDBWrapper;
import com.tomer.ontime.utils.SharedPreferencesHelper;
import com.tomer.ontime.utils.TimeConstants;
import com.tomer.ontime.utils.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class AssignmentsReminderIntentService extends IntentService {

    public static final String TAG = AssignmentsReminderIntentService.class.getSimpleName();
    private static final String REMINDER_NOTIFICATION_TITLE = "Assignments are due soon!";
    public static final String REMINDER_NOTIFICATION_SMALL_MESSAGE = "Swipe down for more details";
    public static final int ASSIGNMENT_NAME_LENGTH_LIMIT = 25;

    private SharedPreferencesHelper sharedPrefsHelper;
    private List<Assignment> todoAssignments;

    public AssignmentsReminderIntentService() {
        super(AssignmentsReminderIntentService.class.getName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OnTimeApplication appContext = (OnTimeApplication) getApplication();
        AssignmentsDBWrapper assignmentsDBWrapper = appContext.getAssignmentsDBWrapper();
        todoAssignments = assignmentsDBWrapper.getTodoAssignments();
        assignmentsDBWrapper.close();
        sharedPrefsHelper = appContext.getSharedPrefsHelper();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Assignments reminder service started running");

        if (!todoAssignments.isEmpty()){
            Set<String> reminderTimes = sharedPrefsHelper.getDueDatesReminderTimes();
            List<Pair<Assignment, String>> assignmentsAndNumOfDaysUntilDue = getAssignmentsAndNumOfDaysUntilDue(reminderTimes);
            if (!assignmentsAndNumOfDaysUntilDue.isEmpty()){
                Intent resultIntent = new Intent(getApplicationContext(), LoginActivity.class);
                Utils.showNotification(getApplicationContext(),
                        PendingIntent.getActivity(getApplicationContext(), 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT),
                        REMINDER_NOTIFICATION_TITLE,
                        REMINDER_NOTIFICATION_SMALL_MESSAGE,
                        buildNotificationStyle(assignmentsAndNumOfDaysUntilDue));
            }
        }

        Log.d(TAG, "Assignments reminder service finished");
    }

    private List<Pair<Assignment, String>> getAssignmentsAndNumOfDaysUntilDue(Set<String> reminderTimes) {
        List<Pair<Assignment, String>> assignmentsAndNumOfDaysUntilDue = new ArrayList<>();
        for (Assignment assignment : todoAssignments){
            Date assignmentDueDate = assignment.getDueDate();
            if (assignmentDueDate == null){
                continue;
            }
            String numOfDaysUntilDue = String.valueOf((assignmentDueDate.getTime() - System.currentTimeMillis()) / TimeConstants.MILLIS_IN_A_DAY);
            if (reminderTimes.contains(numOfDaysUntilDue)){
                assignmentsAndNumOfDaysUntilDue.add(new Pair<>(assignment, numOfDaysUntilDue));
            }
        }
        return assignmentsAndNumOfDaysUntilDue;
    }

    private NotificationCompat.Style buildNotificationStyle(List<Pair<Assignment, String>> assignmentsAndNumberOfDaysUntilDue){
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        for (Pair<Assignment, String> assignmentAndNumOfDaysUntilDue : assignmentsAndNumberOfDaysUntilDue){
            inboxStyle.addLine(formatAssignmentAndNumOfDaysUntilDue(assignmentAndNumOfDaysUntilDue.first, assignmentAndNumOfDaysUntilDue.second));
        }
        return inboxStyle;
    }

    private CharSequence formatAssignmentAndNumOfDaysUntilDue(Assignment assignment, String numOfDaysUntilDue) {
        String courseNum = Utils.getCourseNumFromCourseName(assignment.getCourseName());
        String assignmentName = assignment.getName();
        if (assignmentName.length() > ASSIGNMENT_NAME_LENGTH_LIMIT){
            assignmentName = assignmentName.substring(0, ASSIGNMENT_NAME_LENGTH_LIMIT - 3) + "...";
        }
        return assignmentName
                + (!courseNum.isEmpty() ? "(" + courseNum + ")" : "")
                + " - "
                + (numOfDaysUntilDue.equals("1") ? "Tomorrow!" : "in " + numOfDaysUntilDue + " Days.");
    }

}
