package com.tomer.ontime.services;

import android.app.IntentService;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.PowerManager;

import com.tomer.ontime.utils.TimeConstants;

import java.util.Date;

/**
 * Created by Tomer on 02-Jun-16.
 */
public abstract class WakefulIntentService extends IntentService{
    private static final String CPU_WAKELOCK_NAME_SUFFIX = "-CpuWakelock";
    private static final String WIFI_WAKELOCK_NAME_SUFFIX = "-WifiWakelock";

    private static final long WAIT_FOR_WIFI_TIMEOUT_MILLIS = 15 * TimeConstants.MILLIS_IN_A_SECOND;
    private static final long CPU_WAKELOCK_TIMEOUT_MILLIS = 3 * TimeConstants.MILLIS_IN_A_MINUTE;

    private PowerManager.WakeLock cpuWakelock;
    private WifiManager.WifiLock wifiWakelock;
    private Date waitForWifiTimeoutDate;

    public WakefulIntentService(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String className = getClass().getSimpleName();
        createAndAcquireCPUWakeLock(className);
        createAndAcquireWifiWakeLock(className);
        scanAndReconnectToWifi();
        waitForWifiTimeoutDate = new Date(System.currentTimeMillis() + WAIT_FOR_WIFI_TIMEOUT_MILLIS);
        waitForWifiNetworkToBeFoundOrTimeout();
    }

    private void scanAndReconnectToWifi() {
        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        wifiManager.startScan();
        wifiManager.reconnect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cpuWakelock.release();
        wifiWakelock.release();
    }

    private void createAndAcquireWifiWakeLock(String name) {
        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        wifiWakelock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL, name + WIFI_WAKELOCK_NAME_SUFFIX);
        wifiWakelock.setReferenceCounted(false);
        wifiWakelock.acquire();
    }

    private void createAndAcquireCPUWakeLock(String className) {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        cpuWakelock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, className + CPU_WAKELOCK_NAME_SUFFIX);
        cpuWakelock.setReferenceCounted(false);
        cpuWakelock.acquire(CPU_WAKELOCK_TIMEOUT_MILLIS);
    }

    private void waitForWifiNetworkToBeFoundOrTimeout() {
        while (!isWifiConnected() && new Date().before(waitForWifiTimeoutDate)){
            try {
                Thread.sleep(TimeConstants.MILLIS_IN_A_SECOND);
            } catch (InterruptedException e) {}
        }
    }

    private boolean isWifiConnected(){
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo wifiNetworkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return wifiNetworkInfo.isConnected();
    }
}
