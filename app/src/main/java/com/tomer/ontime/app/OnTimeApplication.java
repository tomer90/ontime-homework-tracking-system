package com.tomer.ontime.app;

import android.app.Application;
import android.preference.PreferenceManager;
import android.util.Log;

import com.tomer.ontime.R;
import com.tomer.ontime.utils.AssignmentsDBWrapper;
import com.tomer.ontime.utils.SharedPreferencesHelper;

/**
 * Created by Tomer on 18-Mar-16.
 */
public class OnTimeApplication extends Application{

    private static final String DB_NAME = "AssignmentsDB";

    private SharedPreferencesHelper sharedPrefsHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPrefsHelper = new SharedPreferencesHelper(this);
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
    }

    public AssignmentsDBWrapper getAssignmentsDBWrapper() {
        return new AssignmentsDBWrapper(this.openOrCreateDatabase(DB_NAME, MODE_APPEND, null));
    }

    public SharedPreferencesHelper getSharedPrefsHelper(){
        return sharedPrefsHelper;
    }
}
