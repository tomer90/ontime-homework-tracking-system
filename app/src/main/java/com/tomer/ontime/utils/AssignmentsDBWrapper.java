package com.tomer.ontime.utils;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;

import org.jsoup.helper.StringUtil;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Tomer on 18-Mar-16.
 */
public class AssignmentsDBWrapper implements Closeable{
    private static final String TABLE_TODO = "todo";
    private static final String TABLE_DONE = "done";
    private static final String C_ASSIGNMENT_JSON = "assignment_json";
    private static final String C_ASSIGNMENT_COURSE_NAME = "course_num";
    private static final String SQL_TABLE_CREATION_STATEMENT_PATTERN = "CREATE TABLE IF NOT EXISTS %s (%s VARCHAR, %s VARCHAR);";

    private SQLiteDatabase assignmentsDB;

    public AssignmentsDBWrapper(SQLiteDatabase assignmentsDB) {
        this.assignmentsDB = assignmentsDB;
        createDBTablesIfNeeded(assignmentsDB);
    }

    public void addTodoAssignments(List<Assignment> assignments) {
        removeExistingAssignmentsFromList(assignments, getTodoAssignments());
        for (Assignment assignment : assignments) {
            assignment.markNotDone();
            insertAssignmentToTable(assignment, TABLE_TODO);
        }
    }

    public void addDoneAssignments(List<Assignment> assignments) {
        removeExistingAssignmentsFromList(assignments, getDoneAssignments());
        for (Assignment assignment : assignments) {
            assignment.markDone();
            insertAssignmentToTable(assignment, TABLE_DONE);
        }
    }

    public void moveAssignmentToDone(Assignment assignment) {
        deleteAssignmentFromTable(assignment, TABLE_TODO);
        assignment.markDone();
        insertAssignmentToTable(assignment, TABLE_DONE);
    }

    public void moveAssignmentToTodo(Assignment assignment) {
        deleteAssignmentFromTable(assignment, TABLE_DONE);
        assignment.markNotDone();
        insertAssignmentToTable(assignment, TABLE_TODO);
    }

    public void deleteTodoAssignment(Assignment assignment){
        deleteAssignmentFromTable(assignment, TABLE_TODO);
    }

    private void removeExistingAssignmentsFromList(List<Assignment> listToRemoveFrom, List<Assignment> existingAssignments) {
        for (Assignment existingAssignment : existingAssignments){
            if (listToRemoveFrom.contains(existingAssignment)){
                Assignment newAssignment = listToRemoveFrom.get(listToRemoveFrom.indexOf(existingAssignment));
                if (compareDates(newAssignment.getDueDate(), existingAssignment.getDueDate()) == 0) {
                    listToRemoveFrom.remove(existingAssignment);
                }
            }
        }
    }

    private void deleteAssignmentFromTable(Assignment assignment, String table) {
        Gson gson = new Gson();
        assignmentsDB.delete(table, C_ASSIGNMENT_JSON + " = ?", new String[]{ gson.toJson(assignment) });
    }

    public List<Assignment> getTodoAssignments() {
        return getAllAssignmentsFromTable(TABLE_TODO);
    }

    public List<Assignment> getDoneAssignments() {
        return getAllAssignmentsFromTable(TABLE_DONE);
    }

    public void clearDBTables() {
        assignmentsDB.execSQL("DROP TABLE IF EXISTS " + TABLE_TODO);
        assignmentsDB.execSQL("DROP TABLE IF EXISTS " + TABLE_DONE);
        createDBTablesIfNeeded(assignmentsDB);
    }

    private void insertAssignmentToTable(Assignment assignment, String table) {
        Gson gson = new Gson();
        ContentValues cv = new ContentValues();
        cv.put(C_ASSIGNMENT_JSON, gson.toJson(assignment));
        cv.put(C_ASSIGNMENT_COURSE_NAME, assignment.getCourseName());
        assignmentsDB.insertWithOnConflict(table, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
    }

    private List<Assignment> getAllAssignmentsFromTable(String table) {
        ArrayList<Assignment> assignments = new ArrayList<>();
        Gson gson = new Gson();
        Cursor results = assignmentsDB.rawQuery("SELECT * FROM " + table, null);
        if (results.getCount() > 0) {
            results.moveToFirst();
            do {
                String assignmentJsonStr = results.getString(results.getColumnIndex(C_ASSIGNMENT_JSON));
                assignments.add(gson.fromJson(assignmentJsonStr, Assignment.class));
            } while (results.moveToNext());
            results.close();
        }
        sortAssignmentsByDueDate(assignments);
        return assignments;
    }

    private void sortAssignmentsByDueDate(ArrayList<Assignment> assignments) {
        Collections.sort(assignments, new Comparator<Assignment>() {
            @Override
            public int compare(Assignment lhs, Assignment rhs) {
                Date lhsDueDate = lhs.getDueDate();
                Date rhsDueDate = rhs.getDueDate();
                return compareDates(lhsDueDate, rhsDueDate);
            }
        });
    }

    private int compareDates(Date lhsDueDate, Date rhsDueDate) {
        if (lhsDueDate == null && rhsDueDate == null){
            return 0;
        }
        if (lhsDueDate == null){
            return 1;
        }
        if (rhsDueDate == null){
            return -1;
        }
        return lhsDueDate.compareTo(rhsDueDate);
    }

    private void createDBTablesIfNeeded(SQLiteDatabase assignmentsDB) {
        String sqlTodoTable = String.format(SQL_TABLE_CREATION_STATEMENT_PATTERN,
                TABLE_TODO, C_ASSIGNMENT_JSON, C_ASSIGNMENT_COURSE_NAME);
        String sqlDoneTable = String.format(SQL_TABLE_CREATION_STATEMENT_PATTERN,
                TABLE_DONE, C_ASSIGNMENT_JSON, C_ASSIGNMENT_COURSE_NAME);
        assignmentsDB.execSQL(sqlTodoTable);
        assignmentsDB.execSQL(sqlDoneTable);
    }

    public void removeAllAssignmentsOfCourse(String courseNum){
        if (StringUtil.isBlank(courseNum)){
            return;
        }
        String deleteStatementPattern = "DELETE FROM %s WHERE " + C_ASSIGNMENT_COURSE_NAME + " LIKE '%%" + courseNum + "%%'";
        assignmentsDB.execSQL(String.format(deleteStatementPattern, TABLE_TODO));
        assignmentsDB.execSQL(String.format(deleteStatementPattern, TABLE_DONE));
    }

    public void close() {
        assignmentsDB.close();
    }

}
