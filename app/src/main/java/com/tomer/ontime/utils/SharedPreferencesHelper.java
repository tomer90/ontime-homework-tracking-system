package com.tomer.ontime.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.tomer.ontime.R;

import org.jsoup.helper.StringUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Tomer on 17-Jun-16.
 */
public class SharedPreferencesHelper {

    private static final String SEMESTERS_TO_CHECK_KEY = "semesters";
    private static final String SHARED_PREFS_DELIM = "-";

    private SharedPreferences prefs;
    private String usernameKey;
    private String passwordKey;
    private String enrolledCoursesKey;
    private String reminderTimesListKey;
    private String shouldDetectEnrolledCoursesKey;

    public SharedPreferencesHelper(Context c) {
        usernameKey = c.getString(R.string.pref_moodle_id_key);
        passwordKey = c.getString(R.string.pref_moodle_password_key);
        enrolledCoursesKey = c.getString(R.string.pref_enrolled_courses_key);
        reminderTimesListKey = c.getString(R.string.pref_reminder_times_list_key);
        shouldDetectEnrolledCoursesKey = c.getString(R.string.pref_detect_enrolled_courses_key);
        prefs = PreferenceManager.getDefaultSharedPreferences(c);
    }

    public String getShouldDetectEnrolledCoursesKey() {
        return shouldDetectEnrolledCoursesKey;
    }

    public String getReminderTimesListKey() {
        return reminderTimesListKey;
    }

    public String getEnrolledCoursesKey() {
        return enrolledCoursesKey;
    }

    public String getPasswordKey() {
        return passwordKey;
    }

    public String getUsernameKey() {
        return usernameKey;
    }

    public Set<String> getDueDatesReminderTimes() {
        Set<String> setToReturn = prefs.getStringSet(reminderTimesListKey, Collections.<String>emptySet());
        return new TreeSet<>(setToReturn);
    }

    public Set<String> getEnrolledCourses() {
        return getStringSet(enrolledCoursesKey);
    }

    public Set<String> getEnrolledCoursesFromEnrolledCoursesString(String enrolledCourses) {
        return convertStringToSet(enrolledCourses, SHARED_PREFS_DELIM);
    }

    public String getEnrolledCoursesAsString() {
        return prefs.getString(enrolledCoursesKey, "");
    }

    public boolean shouldDetectEnrolledCourses() {
        return prefs.getBoolean(shouldDetectEnrolledCoursesKey, true);
    }

    public String getUsername() {
        return prefs.getString(usernameKey, "");
    }

    public String getPassword() {
        return prefs.getString(passwordKey, "");
    }

    public Set<String> getSemestersToCheck() {
        return getStringSet(SEMESTERS_TO_CHECK_KEY);
    }

    public Set<String> getCoursesForSemester(String semester) {
        return getStringSet(semester);
    }

    public void setSemestersToCheck(Set<String> semesters) {
        storeStringSet(SEMESTERS_TO_CHECK_KEY, semesters);
    }

    public void setCoursesForSemester(String semester, Set<String> courses) {
        storeStringSet(semester, courses);
    }

    public void setEnrolledCourses(Set<String> courses) {
        storeStringSet(enrolledCoursesKey, courses);
    }

    public void setUsername(String username){
        storeStringValue(usernameKey, username);
    }

    public void setPassword(String password){
        storeStringValue(passwordKey, password);
    }

    private void storeStringSet(String key, Set<String> set) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String courseNum : set) {
            stringBuilder.append(courseNum);
            stringBuilder.append(SHARED_PREFS_DELIM);
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        }
        storeStringValue(key, stringBuilder.toString());
    }

    private void storeStringValue(String key, String value) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    private Set<String> getStringSet(String key) {
        String enrolledCoursesString = prefs.getString(key, "");
        return convertStringToSet(enrolledCoursesString, SHARED_PREFS_DELIM);
    }

    private Set<String> convertStringToSet(String enrolledCoursesString, String delimiter) {
        return StringUtil.isBlank(enrolledCoursesString) ?
                Collections.<String>emptySet() :
                new TreeSet<>(Arrays.asList(
                        enrolledCoursesString.split(delimiter)
                ));
    }
}
