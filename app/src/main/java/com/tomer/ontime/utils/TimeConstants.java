package com.tomer.ontime.utils;

/**
 * Created by Tomer on 10-May-16.
 */
public class TimeConstants {
    public static final long MILLIS_IN_A_SECOND = 1000;
    public static final long MILLIS_IN_A_MINUTE = 60 * MILLIS_IN_A_SECOND;
    public static final long MILLIS_IN_AN_HOUR = 60 * MILLIS_IN_A_MINUTE;
    public static final long MILLIS_IN_A_DAY = 24 * MILLIS_IN_AN_HOUR;
}
