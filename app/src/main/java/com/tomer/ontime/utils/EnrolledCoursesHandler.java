package com.tomer.ontime.utils;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Tomer on 29-Jun-16.
 */
public class EnrolledCoursesHandler {
    private static final String UG_BASE_URL = "https://ug3.technion.ac.il/rishum/";
    private static final String UG_SCHEDULE_PAGE_URL = UG_BASE_URL + "schedule";
    private static final String UG_LOGIN_PAGE_URL = UG_BASE_URL + "login";
    private static final String UG_SCHEDULE_FOR_SEMESTER_URL_PATTERN = "http://techmvs.technion.ac.il:100/cics/wmn/wmnnut03?OP=WK&SEM=%s";
    private static final String JSOUP_ENROLLED_SEMESTERS_SELECTOR = "input[name=SEM]";
    private static final String JSOUP_ENROLLED_COURSES_SELECTOR = "a[href^=https://ug3.technion.ac.il/rishum/course/]";
    private static final String TAG = EnrolledCoursesHandler.class.getSimpleName();

    private final SharedPreferencesHelper sharedPrefsHelper;
    private final AssignmentsDBWrapper assignmentsDBWrapper;

    public EnrolledCoursesHandler(SharedPreferencesHelper sharedPrefsHelper, AssignmentsDBWrapper assignmentsDBWrapper){
        this.sharedPrefsHelper = sharedPrefsHelper;
        this.assignmentsDBWrapper = assignmentsDBWrapper;
    }

    /**
     * @return Were any course numbers removed from the DB
     */
    public boolean handleEnrolledCourses() {
        if (sharedPrefsHelper.shouldDetectEnrolledCourses()){
            updateUserEnrolledCourses();
        }
        return removeDisenrolledCoursesAssignments(sharedPrefsHelper.getEnrolledCourses());
    }

    private boolean removeDisenrolledCoursesAssignments(Set<String> newEnrolledCourses) {
        Set<String> courseNumsToRemove = new HashSet<>();
        List<Assignment> allAssignments = assignmentsDBWrapper.getTodoAssignments();
        allAssignments.addAll(assignmentsDBWrapper.getDoneAssignments());
        for (Assignment assignment : allAssignments){
            String assignmentCourseNum = Utils.getCourseNumFromCourseName(assignment.getCourseName());
            if (!newEnrolledCourses.contains(assignmentCourseNum)){
                courseNumsToRemove.add(assignmentCourseNum);
            }
        }
        for (String courseNum : courseNumsToRemove){
            assignmentsDBWrapper.removeAllAssignmentsOfCourse(courseNum);
        }
        return !courseNumsToRemove.isEmpty();
    }

    private void updateUserEnrolledCourses() {
        String username = sharedPrefsHelper.getUsername();
        String password = sharedPrefsHelper.getPassword();
        if (StringUtil.isBlank(username) || StringUtil.isBlank(password)){
            return;
        }
        try {
            if (!loginToUG(username, password)){
                return;
            }
            HashSet<String> enrolledCourses = new HashSet<>();
            Set<String> semestersToCheck = getSemestersToCheck();
            sharedPrefsHelper.setSemestersToCheck(semestersToCheck);
            for (String semester : semestersToCheck){
                HashSet<String> enrolledCoursesForSemester = getEnrolledCoursesForSemester(semester);
                enrolledCourses.addAll(enrolledCoursesForSemester);
                sharedPrefsHelper.setCoursesForSemester(semester, enrolledCoursesForSemester);
            }
            sharedPrefsHelper.setEnrolledCourses(enrolledCourses);
        } catch (IOException e) {
            Log.e(TAG, "Failed to get enrolled courses", e);
        }
    }

    private boolean loginToUG(String username, String password) throws IOException {
        Utils.doHttpGet(UG_LOGIN_PAGE_URL);
        Utils.doHttpPost(UG_LOGIN_PAGE_URL, "OP=LI&UID=" + username + "&PWD=" + password + "&Login.x=התחבר");
        return !isUGSleeping(Jsoup.connect(UG_LOGIN_PAGE_URL).get());
    }

    private HashSet<String> getEnrolledCoursesForSemester(String semester) throws IOException {
        HashSet<String> enrolledCourses = new HashSet<>();
        Document semesterSchedulePageDoc = Jsoup.connect(String.format(UG_SCHEDULE_FOR_SEMESTER_URL_PATTERN, semester)).get();
        for (Element e : semesterSchedulePageDoc.select(JSOUP_ENROLLED_COURSES_SELECTOR)){
            if (isEnrolledCourseElement(e)){
                enrolledCourses.add(e.ownText());
            }
        }
        return enrolledCourses;
    }

    private boolean isEnrolledCourseElement(Element e) {
        return Utils.containsOnlyDigits(e.ownText());
    }

    private Set<String> getSemestersToCheck() throws IOException {
        Document schedulePageDoc = Jsoup.connect(UG_SCHEDULE_PAGE_URL).get();
        HashSet<String> foundSemesters = new HashSet<>();
        for (Element semesterElement : schedulePageDoc.select(JSOUP_ENROLLED_SEMESTERS_SELECTOR)){
            foundSemesters.add(semesterElement.attr("value"));
        }
        return foundSemesters;
    }

    private boolean isUGSleeping(Document enrolledCoursesPageDoc) {
        return !enrolledCoursesPageDoc.select("img[src*=sleep.png]").isEmpty();
    }
}
