package com.tomer.ontime.utils;

import java.util.Date;

/**
 * Created by Tomer on 10-Jan-16.
 */
public class Assignment {
    private String name;
    private Date dueDate;
    private String courseName;
    private String semester;   //Only WebCourseAssignmentsGetter cares about this field
    private boolean isDone;

    public Assignment(String name, Date dueDate, String courseName) {
        this.name = name;
        this.dueDate = dueDate;
        this.courseName = courseName;
    }

    public Assignment(String name, Date dueDate, String courseName, String semester) {
        this(name, dueDate, courseName);
        this.semester = semester;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public String getName() {
        return name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void markDone(){
        isDone = true;
    }

    public void markNotDone(){
        isDone = false;
    }

    public boolean isDone(){
        return isDone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Assignment that = (Assignment) o;

        if (isDone != that.isDone) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (courseName != null ? !courseName.equals(that.courseName) : that.courseName != null)
            return false;
        return semester != null ? semester.equals(that.semester) : that.semester == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (courseName != null ? courseName.hashCode() : 0);
        result = 31 * result + (semester != null ? semester.hashCode() : 0);
        result = 31 * result + (isDone ? 1 : 0);
        return result;
    }
}
