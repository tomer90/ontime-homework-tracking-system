package com.tomer.ontime.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.tomer.ontime.R;
import com.tomer.ontime.fragments.AssignmentsBaseFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tomer on 09-Jan-16.
 */
public class Utils {
    public static final int HTTP_DEFAULT_TIMEOUT = 15000;

    public static void sendEmail(String[] recipients, String subject, Activity sendingActivity) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.setType("message/rfc822");
        sendingActivity.startActivity(Intent.createChooser(intent, "Choose an Email client: "));
    }

    public static void showNotification(Context context, PendingIntent onClickIntent,
                                        String title, String text) {
        createAndDisplayNotification(context, onClickIntent, title, text, null);
    }

    public static void showNotification(Context context, PendingIntent onClickIntent,
                                        String title, String text, NotificationCompat.Style style) {
        createAndDisplayNotification(context, onClickIntent, title, text, style);
    }

    private static void createAndDisplayNotification(Context context, PendingIntent onClickIntent, String title, String text, NotificationCompat.Style style) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon);
        notificationBuilder.setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.app_icon)
                .setLargeIcon(icon)
                .setContentTitle(title)
                .setContentText(text)
                .setFullScreenIntent(onClickIntent, false)
                .setContentIntent(onClickIntent)
                .setAutoCancel(true)
                .setStyle(style);
        Notification notification = notificationBuilder.build();
        notificationManager.notify(new Random().nextInt(), notification);
    }

    public static String doHttpGet(String urlStr) throws IOException {
        if (urlStr == null) {
            return null;
        }
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(HTTP_DEFAULT_TIMEOUT);
        conn.setReadTimeout(HTTP_DEFAULT_TIMEOUT);
        conn.setRequestMethod("GET");
        conn.connect();
        String responseHtml = inputStreamToListOfString(conn.getInputStream());
        int responseCode = conn.getResponseCode();
        if (responseCode != 200 && responseCode != 303) {
            conn.disconnect();
            throw new IOException("Bad HTTP response code: " + responseCode);
        }
        conn.disconnect();
        return responseHtml;
    }

    public static String doHttpPost(String urlStr, String payload) throws IOException {
        if (payload == null || urlStr == null) {
            return null;
        }
        if (CookieHandler.getDefault() == null) {
            CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        }
        URL url = new URL(urlStr);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(HTTP_DEFAULT_TIMEOUT);
        conn.setReadTimeout(HTTP_DEFAULT_TIMEOUT);
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setInstanceFollowRedirects(true);
        conn.setRequestProperty("Content-Length", String.valueOf(payload.getBytes().length));
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.connect();
        OutputStream outputStream = conn.getOutputStream();
        outputStream.write(payload.getBytes());
        outputStream.flush();
        outputStream.close();
        String htmlLines = inputStreamToListOfString(conn.getInputStream());
        int responseCode = conn.getResponseCode();
        if (responseCode != 200 && responseCode != 303) {
            conn.disconnect();
            throw new IOException("Bad HTTP response code: " + responseCode);
        }
        conn.disconnect();
        return htmlLines;
    }

    public static String inputStreamToListOfString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuilder linesSB = new StringBuilder();
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            linesSB.append(line);
        }
        reader.close();
        inputStream.close();
        return linesSB.toString();
    }

    /* Returns a list with all the valid dates found in the string, earliest date first */
    public static LinkedList<Date> getValidDatesInString(String str) {
        LinkedList<Date> dates = new LinkedList<>();
        String hourRegex = "[0-2][0-9]:[0-6][0-9]";
        String dateRegex = "((?:(?:[0-2]?\\d{1})|(?:[3][01]{1}))[-:\\/.](?:[0]?[1-9]|[1][012])[-:\\/.](?:(?:[1]{1}\\d{1}\\d{1}\\d{1})|(?:[2]{1}\\d{3})))(?![\\d])";
        String dateAndHourRegex1 = dateRegex + "(\\s*)(\\p{P}*)(\\s*)" + hourRegex;
        String dateAndHourRegex2 = "([1-9]|[1-3][0-9])\\s*(January|February|March|April|May|June|July|August|September|October|November|December)\\s*[0-9][0-9][0-9][0-9]\\s*\\p{P}\\s*" + hourRegex + "\\s*(am|AM|pm|PM)";

        Pattern datePattern = Pattern.compile(dateAndHourRegex1 + "|" + dateAndHourRegex2 + "|" + dateRegex,
                Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher matcher = datePattern.matcher(str);
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        SimpleDateFormat dateTimeFormat2 = new SimpleDateFormat("dd MMMM yyyy KK:mm a", Locale.US);
        SimpleDateFormat dateOnlyFormat = new SimpleDateFormat("dd/MM/yyyy");
        while (matcher.find()) {
            String dateString = matcher.group().replace(",","");
            Date date = formatDate(dateString, Arrays.asList(dateTimeFormat,
                    dateTimeFormat2, dateOnlyFormat));
            if (date != null) {
                dates.add(date);
            }
        }
        Collections.sort(dates);
        return dates;
    }

    private static Date formatDate(String date, List<SimpleDateFormat> formatters) {
        for (SimpleDateFormat formatter : formatters) {
            try {
                return formatter.parse(date);
            } catch (ParseException e) {}
        }
        return null;
    }

    private static boolean isIntentScheduled(Context c, Intent i){
        return PendingIntent.getService(c, 0, i, PendingIntent.FLAG_NO_CREATE) != null;
    }

    public static void scheduleRepeatingIntentIfNotScheduled(Context context, int type, long triggerAtMillis,
                                                             long intervalMillis, Intent intent){
        if (!isIntentScheduled(context, intent)){
            AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmMgr.setRepeating(type, triggerAtMillis, intervalMillis, pendingIntent);
        }
    }

    public static void alertAssignmentsDBChanged(Context context) {
        Intent i = new Intent(AssignmentsBaseFragment.AssignmentsDBChangedListener.ACTION_ASSIGNMENTS_DB_CHANGED);
        context.sendBroadcast(i);
    }

    public static boolean containsOnlyDigits(String text) {
        return text != null && !text.matches(".*[^0-9].*");
    }

    public static String getCourseNumFromCourseName(String courseName) {
        String courseNumRegex = "[0-9][0-9][0-9][0-9][0-9][0-9]";

        Pattern datePattern = Pattern.compile(courseNumRegex, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher matcher = datePattern.matcher(courseName);
        return matcher.find() ? matcher.group() : "";
    }
}
