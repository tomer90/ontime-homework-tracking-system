package com.tomer.ontime.utils;

import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tomer.ontime.R;
import com.tomer.ontime.adapters.AssignmentsRecyclerAdapter;
import com.tomer.ontime.app.OnTimeApplication;

import java.util.Date;

/**
 * Created by Tomer on 09-Jun-16.
 */
public class AssignmentViewHolder extends RecyclerView.ViewHolder {
    private TextView dueTextView;
    private TextView daysTextView;
    private TextView hoursTextView;
    private TextView minsTextView;
    private TextView daysLeftTextView;
    private TextView hoursLeftTextView;
    private TextView minsLeftTextView;
    private Button doneButton;
    private TextView assignmentNameTextView;
    private TextView courseNameTextView;
    private AssignmentsRecyclerAdapter ownerAdapter;
    private FragmentActivity fragmentActivity;

    public AssignmentViewHolder(View v, AssignmentsRecyclerAdapter ownerAdapter, FragmentActivity fragmentActivity) {
        super(v);
        dueTextView = (TextView) v.findViewById(R.id.dueTextView);
        daysTextView = (TextView) v.findViewById(R.id.daysTextView);
        hoursTextView = (TextView) v.findViewById(R.id.hoursTextView);
        minsTextView = (TextView) v.findViewById(R.id.minsTextView);
        daysLeftTextView = (TextView) v.findViewById(R.id.daysLeftTextView);
        hoursLeftTextView = (TextView) v.findViewById(R.id.hoursLeftTextView);
        minsLeftTextView = (TextView) v.findViewById(R.id.minsLeftTextView);
        doneButton = (Button) v.findViewById(R.id.doneButton);
        assignmentNameTextView = (TextView) v.findViewById(R.id.assignmentNameTextView);
        courseNameTextView = (TextView) v.findViewById(R.id.courseNameTextView);
        doneButton.setOnClickListener(new DoneButtonOnClickListener());
        this.ownerAdapter = ownerAdapter;
        this.fragmentActivity = fragmentActivity;
    }

    public void setViewText(String text, int viewId) {
        switch (viewId) {
            case R.id.dueTextView: {
                dueTextView.setText(text);
                break;
            }
            case R.id.daysTextView: {
                daysTextView.setText(text);
                break;
            }
            case R.id.hoursTextView: {
                hoursTextView.setText(text);
                break;
            }
            case R.id.minsTextView: {
                minsTextView.setText(text);
                break;
            }
            case R.id.daysLeftTextView: {
                daysLeftTextView.setText(text);
                break;
            }
            case R.id.hoursLeftTextView: {
                hoursLeftTextView.setText(text);
                break;
            }
            case R.id.minsLeftTextView: {
                minsLeftTextView.setText(text);
                break;
            }
            case R.id.assignmentNameTextView: {
                assignmentNameTextView.setText(text);
                break;
            }
            case R.id.courseNameTextView: {
                courseNameTextView.setText(text);
                break;
            }
            case R.id.doneButton: {
                doneButton.setText(text);
                break;
            }
            default:
                break;
        }
    }

    public void setViewVisibility(int visibility, int textViewId) {
        switch (textViewId) {
            case R.id.dueTextView: {
                dueTextView.setVisibility(visibility);
                break;
            }
            case R.id.daysTextView: {
                daysTextView.setVisibility(visibility);
                break;
            }
            case R.id.hoursTextView: {
                hoursTextView.setVisibility(visibility);
                break;
            }
            case R.id.minsTextView: {
                minsTextView.setVisibility(visibility);
                break;
            }
            case R.id.daysLeftTextView: {
                daysLeftTextView.setVisibility(visibility);
                break;
            }
            case R.id.hoursLeftTextView: {
                hoursLeftTextView.setVisibility(visibility);
                break;
            }
            case R.id.minsLeftTextView: {
                minsLeftTextView.setVisibility(visibility);
                break;
            }
            case R.id.doneButton: {
                doneButton.setVisibility(visibility);
                break;
            }
            default:
                break;
        }
    }

    private class DoneButtonOnClickListener implements View.OnClickListener{
        private static final String IS_ASSIGNMENT_FINISHED_DIALOG_NEGATIVE_BUTTON_TEXT = "No";
        private static final String IS_ASSIGNMENT_FINISHED_DIALOG_MESSAGE_PATTERN = "Did you finish %s?";
        private static final String IS_ASSIGNMENT_NOT_FINISHED_DIALOG_MESSAGE_PATTERN = "Move %s to TODO?";
        private static final String IS_ASSIGNMENT_FINISHED_DIALOG_POSITIVE_BUTTON_TEXT = "Yes";
        public static final String ASSIGNMENT_DUE_DATE_PASSED_MESSAGE = "Cant move to TODO, assignment due date has already passed";

        @Override
        public void onClick(View v) {
            buildIsAssignmentFinishedDialog(ownerAdapter.getItemAt(getAdapterPosition())).show();
        }

        private AlertDialog buildIsAssignmentFinishedDialog(final Assignment assignment) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(fragmentActivity);
            String messagePattern = assignment.isDone() ?
                    IS_ASSIGNMENT_NOT_FINISHED_DIALOG_MESSAGE_PATTERN
                    : IS_ASSIGNMENT_FINISHED_DIALOG_MESSAGE_PATTERN;
            alertDialogBuilder.setMessage(String.format(messagePattern, assignment.getName()));
            alertDialogBuilder.setPositiveButton(IS_ASSIGNMENT_FINISHED_DIALOG_POSITIVE_BUTTON_TEXT,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (assignment.isDone() && hasAssignmentDueDatePassed(assignment.getDueDate())){
                                Toast.makeText(fragmentActivity, ASSIGNMENT_DUE_DATE_PASSED_MESSAGE, Toast.LENGTH_LONG).show();
                                return;
                            }
                            ownerAdapter.removeItemAt(getAdapterPosition());
                            AssignmentsDBWrapper assignmentsDBWrapper = ((OnTimeApplication) fragmentActivity.getApplicationContext()).getAssignmentsDBWrapper();
                            if (assignment.isDone()){
                                assignmentsDBWrapper.moveAssignmentToTodo(assignment);
                            } else {
                                assignmentsDBWrapper.moveAssignmentToDone(assignment);
                            }
                            assignmentsDBWrapper.close();
                            Utils.alertAssignmentsDBChanged(fragmentActivity);
                        }

                        private boolean hasAssignmentDueDatePassed(Date assignmentDueDate){
                            return assignmentDueDate != null
                                    && System.currentTimeMillis() - assignmentDueDate.getTime() >= TimeConstants.MILLIS_IN_A_MINUTE;
                        }
                    });
            alertDialogBuilder.setNegativeButton(IS_ASSIGNMENT_FINISHED_DIALOG_NEGATIVE_BUTTON_TEXT, null);
            alertDialogBuilder.setCancelable(true);
            return alertDialogBuilder.create();
        }
    }
}
