package com.tomer.ontime.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tomer.ontime.fragments.DoneAssignmentsFragment;
import com.tomer.ontime.fragments.TodoAssignmentsFragment;

/**
 * Created by Tomer on 07-Jun-16.
 */
public class TodoDoneFragmentPagerAdapter extends FragmentPagerAdapter {
    private static final int NUM_OF_PAGES = 2;
    public static final String TODO_ASSIGNMENTS_TAB_NAME = "Todo";
    public static final String DONE_ASSIGNMENTS_TAB_NAME = "Done";

    public TodoDoneFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new TodoAssignmentsFragment();
            case 1: return new DoneAssignmentsFragment();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_OF_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: return TODO_ASSIGNMENTS_TAB_NAME;
            case 1: return DONE_ASSIGNMENTS_TAB_NAME;
            default: return null;
        }
    }
}
