package com.tomer.ontime.adapters;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tomer.ontime.R;
import com.tomer.ontime.utils.Assignment;
import com.tomer.ontime.utils.AssignmentViewHolder;
import com.tomer.ontime.utils.TimeConstants;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Tomer on 06-Apr-16.
 */
public class AssignmentsRecyclerAdapter extends RecyclerView.Adapter<AssignmentViewHolder>{

    public static final String DONE_ASSIGNMENT_DUE_TEXT = "Assignment was due on: ";
    public static final String DUE_ASSIGNMENT_DUE_TEXT = "Due in:";
    public static final String NOT_FOR_SUBMISSION_ASSIGNMENT_DUE_TEXT = "Not for submission";
    public static final String DONE_ASSIGNMENT_DONE_BUTTON_TEXT = "UNDO";

    private AssignmentsType assignmentsType;
    private List<Assignment> assignments;
    private FragmentActivity fragmentActivity;

    public enum AssignmentsType{
        TODO, DONE
    }

    public AssignmentsRecyclerAdapter(FragmentActivity fragmentActivity, List<Assignment> assignments,
                                      AssignmentsType assignmentsType) {
        this.assignmentsType = assignmentsType;
        this.assignments = assignments;
        this.fragmentActivity = fragmentActivity;
    }

    @Override
    public AssignmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(fragmentActivity).inflate(R.layout.assignments_recycler_item, parent, false);
        return new AssignmentViewHolder(v, this, fragmentActivity);
    }

    @Override
    public void onBindViewHolder(AssignmentViewHolder holder, int position) {
        Assignment assignment = assignments.get(position);
        holder.setViewText(assignment.getName(), R.id.assignmentNameTextView);
        holder.setViewText(assignment.getCourseName(), R.id.courseNameTextView);
        Date assignmentDueDate = assignment.getDueDate();
        if (assignmentDueDate != null) {
            switch (assignmentsType){
                case TODO: {
                    long dateDiffInMillis = assignmentDueDate.getTime() - System.currentTimeMillis();
                    handleDueAssignment(holder, dateDiffInMillis);
                    break;
                }
                case DONE: {
                    handleDoneAssignment(holder, assignmentDueDate);
                    break;
                }
                default: break;
            }
        } else {
            handleNonDueAssignment(holder);
        }
    }

    @Override
    public int getItemCount() {
        return assignments.size();
    }

    public void removeItemAt(int position) {
        assignments.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, assignments.size());
    }

    public Assignment getItemAt(int position) {
        return assignments.get(position);
    }

    private void handleDoneAssignment(AssignmentViewHolder assignmentViewHolder, Date assignmentDueDate) {
        setViewHolderCountdownViewsVisibility(assignmentViewHolder, View.INVISIBLE);
        assignmentViewHolder.setViewText(DONE_ASSIGNMENT_DONE_BUTTON_TEXT, R.id.doneButton);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String sourceString = DONE_ASSIGNMENT_DUE_TEXT + "<b>" + dateFormatter.format(assignmentDueDate) + "</b>";
        assignmentViewHolder.setViewText(Html.fromHtml(sourceString).toString(), R.id.dueTextView);
    }

    private void setViewHolderCountdownViewsVisibility(AssignmentViewHolder assignmentViewHolder, int visibility) {
        assignmentViewHolder.setViewVisibility(visibility, R.id.daysTextView);
        assignmentViewHolder.setViewVisibility(visibility, R.id.hoursTextView);
        assignmentViewHolder.setViewVisibility(visibility, R.id.minsTextView);
        assignmentViewHolder.setViewVisibility(visibility, R.id.daysLeftTextView);
        assignmentViewHolder.setViewVisibility(visibility, R.id.hoursLeftTextView);
        assignmentViewHolder.setViewVisibility(visibility, R.id.minsLeftTextView);
    }

    private void handleNonDueAssignment(AssignmentViewHolder assignmentViewHolder) {
        setViewHolderCountdownViewsVisibility(assignmentViewHolder, View.INVISIBLE);
        assignmentViewHolder.setViewText(NOT_FOR_SUBMISSION_ASSIGNMENT_DUE_TEXT, R.id.dueTextView);
        if (assignmentsType.equals(AssignmentsType.DONE)){
            assignmentViewHolder.setViewText(DONE_ASSIGNMENT_DONE_BUTTON_TEXT, R.id.doneButton);
        }
    }

    private void handleDueAssignment(AssignmentViewHolder assignmentViewHolder, long dateDiffInMillis){
        setViewHolderCountdownViewsVisibility(assignmentViewHolder, View.VISIBLE);
        assignmentViewHolder.setViewText(DUE_ASSIGNMENT_DUE_TEXT, R.id.dueTextView);
        updateViewHolderCountdownViews(assignmentViewHolder, dateDiffInMillis);
    }

    private void updateViewHolderCountdownViews(AssignmentViewHolder assignmentViewHolder, long dateDiffInMillis) {
        long daysLeft = dateDiffInMillis / TimeConstants.MILLIS_IN_A_DAY;
        dateDiffInMillis -= daysLeft * TimeConstants.MILLIS_IN_A_DAY;
        long hoursLeft = dateDiffInMillis / TimeConstants.MILLIS_IN_AN_HOUR;
        dateDiffInMillis -= hoursLeft * TimeConstants.MILLIS_IN_AN_HOUR;
        long minutesLeft = dateDiffInMillis / TimeConstants.MILLIS_IN_A_MINUTE;

        assignmentViewHolder.setViewText(String.valueOf(daysLeft), R.id.daysLeftTextView);
        assignmentViewHolder.setViewText(String.valueOf(hoursLeft), R.id.hoursLeftTextView);
        assignmentViewHolder.setViewText(String.valueOf(minutesLeft), R.id.minsLeftTextView);
    }

}
