package com.tomer.ontime.assignments_getters;

import android.util.Log;
import android.util.Pair;

import com.tomer.ontime.utils.Assignment;
import com.tomer.ontime.utils.SharedPreferencesHelper;
import com.tomer.ontime.utils.Utils;

import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * This class returns a list of {@link Assignment} from the moodle for the given user.
 *
 * Created by Tomer on 10-Jan-16.
 */
public class MoodleAssignmentsGetter implements IAssignmentsGetter{
    public static final String MOODLE_URL = "https://moodle.technion.ac.il/";
    private static final String MOODLE_LOGIN_URL = MOODLE_URL + "login/index.php";
    private static final String ASSIGNMENT_PAGE_LINK_REGEX = ".*\\/mod\\/(?!.*(resource|forum|clean|page|url|folder|data|lightboxgallery|feedback)).*\\/view.*";
    private static final String JSOUP_COURSE_PAGE_LINK_SELECTOR = "[href*=course/view]:not([title])";

    public List<Assignment> getAssignments(SharedPreferencesHelper sharedPrefsHelper) {
        ArrayList<Assignment> assignments = new ArrayList<>();
        String username = sharedPrefsHelper.getUsername();
        String password = sharedPrefsHelper.getPassword();
        if (!StringUtil.isBlank(username) && !StringUtil.isBlank(password)){
            try {
                List<Pair<String, String>> moodleCoursesLinksAndNames = getCoursePagesLinksAndNames(username, password,
                        sharedPrefsHelper.getEnrolledCourses());
                for (Pair<String, String> courseLinkAndName : moodleCoursesLinksAndNames) {
                    assignments.addAll(getAssignmentsFromCoursePage(courseLinkAndName.first, courseLinkAndName.second));
                }
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Failed to get assignments from Moodle", e);
            }
        }
        return assignments;
    }

    private Collection<? extends Assignment> getAssignmentsFromCoursePage(String coursePageLink, String courseName) throws IOException {
        ArrayList<Assignment> assignments = new ArrayList<>();
        String coursePageHtml = Utils.doHttpGet(coursePageLink);
        for (Pair<String, String> assignmentPageLinkAndName : getAssignmentsPagesLinksAndNames(coursePageHtml)) {
            String assignmentPageHtml = Utils.doHttpGet(assignmentPageLinkAndName.first);
            LinkedList<Date> validDates = Utils.getValidDatesInString(assignmentPageHtml);
            if (!validDates.isEmpty()) {
                assignments.add(new Assignment(assignmentPageLinkAndName.second, validDates.getLast(), courseName));
            }
        }
        return assignments;
    }

    private List<Pair<String, String>> getAssignmentsPagesLinksAndNames(String coursePageHtml) {
        ArrayList<Pair<String, String>> assignmentsPagesLinksAndNames = new ArrayList<>();
        Document coursePageDoc = Jsoup.parse(coursePageHtml);
        HashSet<String> foundLinks = new HashSet<>();
        for (Element assignmentPageLinkElement : coursePageDoc.getElementsByAttributeValueMatching("href", ASSIGNMENT_PAGE_LINK_REGEX)) {
            String assignmentPageLink = assignmentPageLinkElement.attr("href");
            if (foundLinks.contains(assignmentPageLink)){
                continue;
            }
            String elementText = assignmentPageLinkElement.text().toLowerCase();
            if (isAssignmentValid(elementText)) {
                assignmentsPagesLinksAndNames.add(new Pair<>(assignmentPageLink, getAssignmentName(assignmentPageLinkElement)));
            }
            foundLinks.add(assignmentPageLink);
        }
        return assignmentsPagesLinksAndNames;
    }

    private boolean isAssignmentValid(String assignmentName) {
        return !assignmentName.contains("ערעור")
                && !assignmentName.contains("appeal");
    }

    private String getAssignmentName(Element assignmentPageLinkElement) {
        Element assignmentNameNode = assignmentPageLinkElement.getElementsByAttributeValue("class", "instancename").first();
        return assignmentNameNode != null ? assignmentNameNode.textNodes().get(0).text() : "";
    }

    private List<Pair<String, String>> getCoursePagesLinksAndNames(String username,
                                                                     String password,
                                                                     Set<String> enrolledCourses) throws IOException {
        loginToMoodle(username, password);
        Document moodleMainPageDoc = Jsoup.parse(Utils.doHttpGet(MOODLE_URL));
        List<Pair<String,String>> moodleCoursesLinksAndNames = new ArrayList<>();
        HashSet<String> foundLinks = new HashSet<>();
        for (Element e : moodleMainPageDoc.select(JSOUP_COURSE_PAGE_LINK_SELECTOR)) {
            String coursePageLink = e.attr("href");
            if (foundLinks.contains(coursePageLink)){
                continue;
            }
            String courseName = e.text();
            if (isCourseValid(enrolledCourses, courseName.substring(0,6))){
                moodleCoursesLinksAndNames.add(new Pair<>(coursePageLink, courseName));
            }
            foundLinks.add(coursePageLink);
        }
        return moodleCoursesLinksAndNames;
    }

    private boolean isCourseValid(Set<String> enrolledCourses, String courseNum) {
        return enrolledCourses.contains(courseNum)
                && !WebCourseAssignmentsGetter.isWebcourseCourse(courseNum);
    }

    public static void loginToMoodle(String username, String password) throws IOException {
        Utils.doHttpPost(MOODLE_LOGIN_URL, buildLoginRequestPayload(username, password));
    }

    private static String buildLoginRequestPayload(String username, String password) {
        StringBuilder paramsSB = new StringBuilder("username=");
        paramsSB.append(username);
        paramsSB.append("&");
        paramsSB.append("password=");
        paramsSB.append(password);
        return paramsSB.toString();
    }
}
