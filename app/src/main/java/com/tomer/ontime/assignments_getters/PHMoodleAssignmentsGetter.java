package com.tomer.ontime.assignments_getters;

import android.util.Log;
import android.util.Pair;

import com.tomer.ontime.utils.Assignment;
import com.tomer.ontime.utils.SharedPreferencesHelper;
import com.tomer.ontime.utils.Utils;

import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tomer on 12-Mar-16.
 */
public class PHMoodleAssignmentsGetter implements IAssignmentsGetter{
    private static final String PHMOODLE_URL = "http://phstudy.technion.ac.il/phmoodle/";
    private static final String PHMOODLE_LOGIN_URL = PHMOODLE_URL + "login/index.php";
    private static final long FIVE_MONTHS_MILLIS = 13392000000L;
    private static final String JSOUP_COURSE_PAGE_LINK_SELECTOR = "[href*=course/view]:not(:has(img)):matches(.*\\(.*)";
    private static final String ASSIGNMENT_PAGE_LINK_REGEX = ".*\\/mod\\/(?!.*(resource|forum|clean|page|url|folder|data|lightboxgallery|feedback)).*\\/view.*";

    public List<Assignment> getAssignments(SharedPreferencesHelper sharedPrefsHelper) {
        ArrayList<Assignment> assignments = new ArrayList<>();
        String username = sharedPrefsHelper.getUsername();
        String password = sharedPrefsHelper.getPassword();
        if (!StringUtil.isBlank(username) && !StringUtil.isBlank(password)){
            try {
                List<Pair<String, String>> phmoodleCoursesLinksAndNames = getCoursePagesLinksAndNames(username, password);
                for (Pair<String, String> courseLinkAndName : phmoodleCoursesLinksAndNames) {
                    assignments.addAll(getAssignmentsFromCoursePage(courseLinkAndName.first,
                            courseLinkAndName.second,
                            sharedPrefsHelper.getEnrolledCourses()));
                }
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Failed to get assignments from PHMoodle", e);
            }
        }
        return assignments;
    }

    private List<Pair<String, String>> getCoursePagesLinksAndNames(String username, String password) throws IOException {
        Utils.doHttpPost(PHMOODLE_LOGIN_URL, buildLoginRequestPayload(username, password));
        Document phmoodleMainPageDoc = Jsoup.parse(Utils.doHttpGet(PHMOODLE_URL));
        List<Pair<String,String>> phmoodleCoursesLinksAndNames = new ArrayList<>();
        HashSet<String> foundLinks = new HashSet<>();
        for (Element e : phmoodleMainPageDoc.select(JSOUP_COURSE_PAGE_LINK_SELECTOR)) {
            String coursePageLink = e.attr("href");
            if (foundLinks.contains(coursePageLink)){
                continue;
            }
            phmoodleCoursesLinksAndNames.add(new Pair<>(coursePageLink, e.text()));
            foundLinks.add(coursePageLink);
        }
        return phmoodleCoursesLinksAndNames;
    }

    private String getAssignmentName(Element assignmentPageLinkElement) {
        Element assignmentNameNode = assignmentPageLinkElement.getElementsByTag("span").first();
        if (assignmentNameNode == null){
            return assignmentPageLinkElement.text();
        }
        List<TextNode> textNodes = assignmentNameNode.textNodes();
        return !textNodes.isEmpty() ? textNodes.get(0).text() : assignmentPageLinkElement.text();
    }


    private Collection<? extends Assignment> getAssignmentsFromCoursePage(String coursePageLink,
                                                                            String courseName,
                                                                            Set<String> enrolledCourses) throws IOException {
        ArrayList<Assignment> assignments = new ArrayList<>();
        String coursePageHtml = Utils.doHttpGet(coursePageLink);
        String courseNum = getCourseNum(coursePageHtml);
        if (!StringUtil.isBlank(courseNum)){
            courseName = courseNum + " - " + courseName;
        }
        if (enrolledCourses.contains(courseNum)){
            for (Pair<String, String> assignmentPageLinkAndName : getAssignmentsPagesLinksAndNames(coursePageHtml)) {
                String assignmentPageHtml = Utils.doHttpGet(assignmentPageLinkAndName.first);
                LinkedList<Date> validDates = Utils.getValidDatesInString(assignmentPageHtml);
                if (!validDates.isEmpty()) {
                    Date dueDate = getDueDate(validDates);
                    assignments.add(new Assignment(assignmentPageLinkAndName.second, dueDate, courseName));
                }
            }
        }
        return assignments;
    }

    private String getCourseNum(String coursePageHtml) {
        Pattern datePattern = Pattern.compile("[0-9][0-9][0-9][0-9][0-9][0-9][a-zA-Z][0-9][0-9]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = datePattern.matcher(coursePageHtml);
        if (matcher.find()){
            return matcher.group().substring(0, 6);
        }
        return "";
    }

    private Date getDueDate(LinkedList<Date> validDates) {
        for (int i=0; i < validDates.size() - 1; i++){
            long diff = validDates.get(i+1).getTime() - validDates.get(i).getTime();
            if (diff > FIVE_MONTHS_MILLIS){
                return validDates.get(i);
            }
        }
        return validDates.getLast();
    }

    private List<Pair<String, String>> getAssignmentsPagesLinksAndNames(String coursePageHtml) {
        ArrayList<Pair<String, String>> assignmentsPagesLinksAndNames = new ArrayList<>();
        Document coursePageDoc = Jsoup.parse(coursePageHtml);
        HashSet<String> foundLinks = new HashSet<>();
        for (Element assignmentPageLinkElement : coursePageDoc.getElementsByAttributeValueMatching("href", ASSIGNMENT_PAGE_LINK_REGEX)) {
            String assignmentPageLink = assignmentPageLinkElement.attr("href");
            if (foundLinks.contains(assignmentPageLink)){
                continue;
            }
            String elementText = assignmentPageLinkElement.text().toLowerCase();
            if (isAssignmentValid(elementText)) {
                assignmentsPagesLinksAndNames.add(new Pair<>(assignmentPageLink, getAssignmentName(assignmentPageLinkElement)));
            }
            foundLinks.add(assignmentPageLink);
        }
        return assignmentsPagesLinksAndNames;
    }

    private boolean isAssignmentValid(String assignmentName) {
        return !assignmentName.contains("ערעור")
                && !assignmentName.contains("appeal");
    }

    private String buildLoginRequestPayload(String username, String password) {
        StringBuilder paramsSB = new StringBuilder("username=");
        paramsSB.append(username);
        paramsSB.append("&");
        paramsSB.append("password=");
        paramsSB.append(password);
        return paramsSB.toString();
    }
}
