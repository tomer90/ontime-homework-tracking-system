package com.tomer.ontime.assignments_getters;

import com.tomer.ontime.utils.Assignment;
import com.tomer.ontime.utils.SharedPreferencesHelper;

import java.util.List;

/**
 * Created by Tomer on 27-Mar-16.
 */
public interface IAssignmentsGetter {
    List<Assignment> getAssignments(SharedPreferencesHelper sharedPrefsHelper);
}
