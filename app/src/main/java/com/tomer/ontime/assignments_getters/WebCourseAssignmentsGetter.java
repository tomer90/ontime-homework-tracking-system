package com.tomer.ontime.assignments_getters;

import android.util.Log;
import android.util.Pair;

import com.tomer.ontime.utils.Assignment;
import com.tomer.ontime.utils.SharedPreferencesHelper;
import com.tomer.ontime.utils.Utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tomer on 11-Mar-16.
 */
public class WebCourseAssignmentsGetter implements IAssignmentsGetter{
    private static final String WEBCOURSE_BASE_URL = "https://webcourse.cs.technion.ac.il/";
    private static final String ASSIGNMENTS_PAGE_URL_PATH = "/en/hw.html";
    private static final String JSOUP_ASSIGNMENTS_FOLDER_SELECTOR = "td > a[href$=.html]:not(:has(img)):matches(.+)";
    private static final String JSOUP_ASSIGNMENT_ELEMENT_SELECTOR_PATTERN = "table:has([href^=/%s/%s/hw/]):not(:has(table))";   //first string should be the course number and the second is the semester
    private static final String JSOUP_ASSIGNMENT_ELEMENT_SECONDARY_SELECTOR = "tbody:has(h2)";
    private static final String JSOUP_COURSE_SEMESTER_SELECTOR = "BASE[href]";

    public List<Assignment> getAssignments(SharedPreferencesHelper sharedPrefsHelper) {
        ArrayList<Assignment> assignments = new ArrayList<>();
        for (Pair<String, String> courseNumberAndSemester : getWebcourseCoursesNumbersAndSemesters(sharedPrefsHelper)) {
            try {
                assignments.addAll(
                        getAssignmentsForCourse(
                            courseNumberAndSemester.first,
                            courseNumberAndSemester.second,
                            sharedPrefsHelper.shouldDetectEnrolledCourses()
                        )
                );
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), "Failed to get assignments for course " + courseNumberAndSemester.first, e);
            }
        }
        return assignments;
    }

    private List<Assignment> getAssignmentsForCourse(String courseNum, String semester, boolean detectEnrolledCourses) throws IOException {
        ArrayList<Assignment> courseAssignments = new ArrayList<>();
        Pair<String, String> courseSemesterAndName = getCourseSemesterAndName(courseNum);
        if (detectEnrolledCourses){
            courseSemesterAndName = new Pair<>(semester, courseSemesterAndName.second);
        }
        String courseAssignmentsPageHtml = Utils.doHttpGet(WEBCOURSE_BASE_URL + courseNum
                + "/" + courseSemesterAndName.first + ASSIGNMENTS_PAGE_URL_PATH);
        Document courseAssignmentsPageDoc = Jsoup.parse(courseAssignmentsPageHtml);
        Elements folderElements = getFolderElementsFromDoc(courseAssignmentsPageDoc);
        if (!folderElements.isEmpty()) {
            for (Element folder : folderElements) {
                Document folderPageDoc = getFolderPageDoc(courseNum, courseSemesterAndName, folder);
                courseAssignments.addAll(getAssignmentsFromDoc(courseSemesterAndName, folderPageDoc, courseNum));
            }
        } else {
            courseAssignments.addAll(getAssignmentsFromDoc(courseSemesterAndName, courseAssignmentsPageDoc, courseNum));
        }
        return courseAssignments;
    }

    private Document getFolderPageDoc(String courseNum, Pair<String, String> courseSemesterAndName, Element folder) throws IOException {
        String folderLinkSuffix = folder.attr("href");
        String folderPageHtml = Utils.doHttpGet(WEBCOURSE_BASE_URL + courseNum
                + "/" + courseSemesterAndName.first + "/en/" + folderLinkSuffix);
        return Jsoup.parse(folderPageHtml);
    }

    private Elements getFolderElementsFromDoc(Document doc) {
        return doc.select(JSOUP_ASSIGNMENTS_FOLDER_SELECTOR);
    }

    private ArrayList<Assignment> getAssignmentsFromDoc(Pair<String, String> courseSemesterAndName,
                                                        Document doc, String courseNum) {
        ArrayList<Assignment> assignments = new ArrayList<>();
        String assignmentSelector = String.format(JSOUP_ASSIGNMENT_ELEMENT_SELECTOR_PATTERN, courseNum, courseSemesterAndName.first);
        Elements assignmentElements = doc.select(assignmentSelector);
        if (assignmentElements.isEmpty()){
            assignmentElements = doc.select(JSOUP_ASSIGNMENT_ELEMENT_SECONDARY_SELECTOR);
        }
        for (Element assignmentElement : assignmentElements) {
            Elements spanElements = assignmentElement.select("h2 > SPAN");
            if (spanElements.isEmpty()) {
                continue;
            }
            String assignmentName = spanElements.get(0).text();
            if (assignmentName.isEmpty()){
                assignmentName = spanElements.get(0).attr("DATA-LANG-en");
            }
            LinkedList<Date> validDates = Utils.getValidDatesInString(assignmentElement.toString());
            Date assignmentDueDate = validDates.isEmpty() ? null : validDates.getLast();
            if (isAssignmentValid(assignmentName, assignmentDueDate)) {
                assignments.add(new Assignment(assignmentName, assignmentDueDate, courseSemesterAndName.second, courseSemesterAndName.first));
            }
        }
        return assignments;
    }

    private boolean isAssignmentValid(String assignmentName, Date assignmentDueDate) {
        return !(assignmentDueDate == null && doesNotContainDigits(assignmentName));
    }

    private Pair<String, String> getCourseSemesterAndName(String courseNumber) throws IOException {
        String courseHomePageHtml = Utils.doHttpGet(WEBCOURSE_BASE_URL + courseNumber);
        Document courseHomePageDoc = Jsoup.parse(courseHomePageHtml);
        String courseSemester = courseHomePageDoc.select(JSOUP_COURSE_SEMESTER_SELECTOR).attr("href").split("/")[2];
        String courseName = courseHomePageDoc.getElementsByTag("title").text().split(", ")[0];
        return new Pair<>(courseSemester, courseName);
    }

    private List<Pair<String, String>> getWebcourseCoursesNumbersAndSemesters(SharedPreferencesHelper sharedPrefsHelper) {
        ArrayList<Pair<String, String>> webCourseCourseNumbersAndSemesters = new ArrayList<>();
        if (sharedPrefsHelper.shouldDetectEnrolledCourses()){
            for (String semester : sharedPrefsHelper.getSemestersToCheck()) {
                String webCourseSemester = convertSemesterToWebCourseSemester(semester);
                for (String courseNum : sharedPrefsHelper.getCoursesForSemester(semester)){
                    if (isWebcourseCourse(courseNum)) {
                        webCourseCourseNumbersAndSemesters.add(new Pair<>(courseNum, webCourseSemester));
                    }
                }
            }
        } else {
            for (String courseNum : sharedPrefsHelper.getEnrolledCourses()){
                if (isWebcourseCourse(courseNum)) {
                    webCourseCourseNumbersAndSemesters.add(new Pair<>(courseNum, ""));
                }
            }
        }
        return webCourseCourseNumbersAndSemesters;
    }

    private String convertSemesterToWebCourseSemester(String semester){
        StringBuilder webCourseSB = new StringBuilder();
        String year = semester.substring(0, 4);
        String nextYear = String.valueOf(Integer.parseInt(year) + 1);
        String semesterNum = semester.substring(4, 6);
        switch(semesterNum){
            case "01": {
                webCourseSB.append("Winter");
                webCourseSB.append(year);
                webCourseSB.append("-");
                webCourseSB.append(nextYear);
                break;
            }
            case "02": {
                webCourseSB.append("Spring");
                webCourseSB.append(nextYear);
                break;
            }
            case "03": {
                webCourseSB.append("Summer");
                webCourseSB.append(nextYear);
                break;
            }
            default: break;
        }
        return webCourseSB.toString();
    }

    public static boolean doesNotContainDigits(String text){
        for (int index=0; index < text.length(); index++){
            if (Character.isDigit(text.codePointAt(index))){
                return false;
            }
        }
        return true;
    }

    public static boolean isWebcourseCourse(String course) {
        return course.startsWith("234") || course.startsWith("236");
    }
}
